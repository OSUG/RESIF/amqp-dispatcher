# AMQP dispatcher

## Installation

This is only a standalone script. Just install the dependencies :

    pip install -r requirements.txt

## Prepare configuration

Write a yaml file like the following example and pass it as an argument to the dispatcher script.

``` yaml
---
# The label of the message property to look for (AMQP1.0 spec)
# This tells the dispatcher to look at the value of property "type" 
# To determine the list of queues to dispatch to
messageProperty: type
# Define the property values and the list of queues to send the message to 
dispatchQueuesByProperty:
  seed-data:
    - seedtree-update
    - seedpsd-data
    - wfcatalog
  station-metadata:
    - seedpsd-metadata
```

## Run

    AMQP_QUEUE=resif-amqp.u-ga.fr:5672/post-integration python amqp_dispatcher.py config.yaml

## License

`amqp-dispatcher` is distributed under the terms of the GPLv3 licence.
