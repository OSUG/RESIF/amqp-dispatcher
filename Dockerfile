FROM python:3.10-slim as builder
RUN apt-get update && apt-get install -y build-essential
COPY requirements.txt /
RUN python3 -m pip install -r /requirements.txt

FROM python:3.10-slim
COPY --from=builder /usr/local/lib/python3.10/site-packages /usr/local/lib/python3.10/site-packages
WORKDIR /app
COPY amqp_dispatcher/amqp_dispatcher.py .
COPY config_sample.yaml ./config.yaml
CMD ./amqp_dispatcher.py config.yaml
