#!/usr/bin/env python3

import logging
import os
import sys
import yaml

from proton.handlers import MessagingHandler
from proton import Message
from proton.reactor import Container
from proton.utils import BlockingConnection

# script behavior depends on RUNMODE variable
RUNMODE = os.environ["RUNMODE"]

# Logger configuration
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger('amqp-dispatcher')
logger.setLevel(level=logging.INFO if RUNMODE == "production" else logging.DEBUG)

# AMQP broker
AMQP_QUEUE = os.environ.get("AMQP_QUEUE", "resif-amqp.u-ga.fr:5672/post-integration")

class Dispatcher(MessagingHandler):
    """
    Receiver, connecting to a queue, waiting for new messages.
    """
    def __init__(self, url, message_property="type", dispatch_conf={}):
        """
        params:
        url: Full URL to the AMQP queue to connect to
        message_property: the name of the property to look for in the messages
        dispatch_conf: Dictionary mapping a property value to a list of destination queues

        Exceptions:
        This constructor checks that dispatch_conf is a dictionary of lists
        """
        super(Dispatcher, self).__init__()
        self.url = url
        self.hostname = url.split('/')[0]
        self.message_property = message_property
        self.dispatch_conf = dispatch_conf
        if len(self.dispatch_conf.keys()) < 1:
            raise ValueError("No keys in dispatch_cont")
        for k,v in self.dispatch_conf.items():
            if type(v) != list:
                raise ValueError(f"Error in configuration for type {k}, {v} should be a list")

    def on_start(self, event):
        logger.info("Listening on %s", self.url)
        event.container.create_receiver(self.url)
        # On pourrait ici ouvrir une connexion sur chaque queue de sortie configurée et les maintenir

    def on_message(self, event):
        logger.info(f"Receiving {event.message.body} with properties {event.message.properties}")
        if self.message_property in event.message.properties.keys():
            for k,v in self.dispatch_conf.items():
                if event.message.properties[self.message_property] == k:
                    logger.info(f"Sending message to queues {v}, to server {self.hostname}")
                    connection = BlockingConnection(self.hostname)
                    for queue in v:
                        try:
                            sender = connection.create_sender(queue)
                            message = Message(durable=True, body=event.message.body)
                            logger.debug(f"Forwarding message to queue {queue}")
                            sender.send(message)
                            sender.close()
                        except Exception as e:
                            logger.error(f"Queue {queue} does not exist ?")
                            logger.error(type(e))
                            logger.error(e)
                            next
                    connection.close()
        else:
            logger.warning(f"No properties in message {event.message.body}. Dropping.")

    def on_connection_closed(self, event):
        logger.debug("Event connection closed")


### Code snippet to send a message
# connection = BlockingConnection(AMQP_SERVER)
# sender = connection.create_sender(QUEUE_SEEDTREE_UPDATE)
# message = Message(durable=True, body=content)
# logger.info("forwarding message to queue %s", QUEUE_SEEDTREE_UPDATE)
# sender.send(message)

if __name__ == "__main__":
    logger.info("Starting")
    try:
        with open(sys.argv[1],'r') as file:
            config = yaml.safe_load(file)
    except FileNotFoundError as e:
        logger.error(e)
        sys.exit(1)
    except yaml.YAMLError as e:
        logger.error(e)
        sys.exit(1)

    try:
        logger.debug(config)
        logger.info(f"Will dispatch messages by property '{config['messageProperty']}'")
        logger.info(f"Dispatch configuration: {config['dispatchQueuesByProperty']}")
    except Exception as e:
        logger.error("Configuration error.")
        sys.exit(1)

    logger.info(f"Trying to connect to {AMQP_QUEUE}")
    Container(Dispatcher(AMQP_QUEUE,
                         message_property=config['messageProperty'],
                         dispatch_conf=config['dispatchQueuesByProperty'])
              ).run()
    logger.info("amqp worker quitting")

# Configuration sample
# ---
# messageProperty: type
# dispatchQueuesByProperty:
#   seedpsd-data:
#     - seedtree-update
#     - seedpsd-data
#     - wfcatalog
#   station-metadata:
#     - seedpsd-metadata
#
