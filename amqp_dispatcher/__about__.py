# SPDX-FileCopyrightText: 2022-present Jonathan Schaeffer <jonathan.schaeffer@cnrs.fr>
#
# SPDX-License-Identifier: MIT
__version__ = '0.0.1'
